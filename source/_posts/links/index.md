---
title: 友情链接
permalink: /links/
notshow: true
comments: true
date: 2021/5/1 00:00:01
layout: py
---

> 这场青春值得骄傲，
> 鼓励我们微笑和奔跑；
>成长路上，
>陪伴让我们扬起嘴角。
>      ——《这场青春值得骄傲》

## 友情链接

<!-- more -->

- [![Micraow Blog](https://cdn.jsdelivr.net/gh/Micraow/pics/favicon.png)](https://msblog.ml "Micraow Blog")
- [![李天星的网站](https://ss.caozhiming.tk//img//litianxing-logo.png)](http://82.156.235.117 "李天星的网站")

## 交换链接

请在你的网站上添加如下友情链接：

> 名称：曹智铭的网站
> 链接：https://blog.caozhiming.tk/
> 图片：https://cdn.jsdelivr.net/gh/cao-zhiming/ss-caozhimingtk@latest/img/logo.png
> 描述：长期潜伏于caozhiming.tk的碳基生物。

然后在下方按照相同的格式评论你的网站的相关内容。我会尽快审核并添加。
