---
title: 关于
permalink: /about/
notshow: true
comments: false
date: 2021/5/1 00:00:01
layout: about
---

## 我是谁？

我叫曹智铭，目前就读于[人大附中翠微学校](http://www.rdfzcw.cn/)的初中部。
<!-- more -->

## 我在哪儿？

我在互联网上的许多角落都有足迹。大部分情况，我的账号的用户名或/和昵称为```曹智铭```或者```caozhiming```。当然，我也可能用了其他名称，
例如在GitHub上我是[cao-zhiming](http://github.com/cao-zhiming)（昵称是“曹智铭”）；在Twitter上我是[caozhiming2020](http://twitter.com/caozhiming2020)，等。

## 我在干（可以干）什么？

<details><summary>我可以干这些：</summary>
<li>数学：★★★☆☆</li><li>英语：★★★★☆</li><li>前端：★★★☆☆</li><li>PHP：★★☆☆☆</li><li>Git：★★★☆☆</li></details>

## 我该如何信任你？

我是```caozhiming.tk```的合法所有者，并已经在[Keybase](http://keybase.io)存储了pgp key。